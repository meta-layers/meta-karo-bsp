#!/bin/bash

pushd /workdir/build/karo-imx6ul-txul/tmp/deploy/images/karo-imx6ul-txul

#SOURCE_1="core-image-minimal-imx6q-phytec-mira-rdk-nand-*.rootfs.wic*"
SOURCE_2="core-image-minimal-bfe-karo-imx6ul-txul-*.rootfs.wic*"
TARGET_1="student@192.168.42.60:/home/student/projects/karo-imx6ul-txul/tmp/deploy/images/karo-imx6ul-txul"

#scp -r ${SOURCE_1} ${TARGET_1}
scp -r ${SOURCE_2} ${TARGET_1}

popd
