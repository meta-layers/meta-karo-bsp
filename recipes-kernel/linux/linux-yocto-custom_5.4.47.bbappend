# linux-yocto-custom_x.x.x.bb 
# attempt to have only kernel version related stuff in here

DEFCONFIGPATH="${THISDIR}/5.4.x/defconfig"
FILESEXTRAPATHS_prepend := "${DEFCONFIGPATH}:"

# we need this for this kernel config
DEPENDS += "lzop-native"

# --> custom defconfig hardcoded
# Does not work as a patch:
# ERROR: linux-yocto-custom-5.4.47-custom-ml-up+gitAUTOINC+fd8cd8ac94-r0 
# do_kernel_metadata: A KBUILD_DEFCONFIG 'karo_imx6ul_txul_5011_defconfig' 
# was specified, but not present in the source tree
#SRC_URI += "\
#           file://0001-karo_imx6ul_txul_5011-custom-defconfig.patch \
#           "
#
#SRC_URI += "\
#            file://karo_imx6ul_txul_5011_defconfig \
#           "
#do_kernel_metadata_prepend() {
#        cp ${WORKDIR}/karo_imx6ul_txul_5011_defconfig ${S}/arch/arm/configs/karo_imx6ul_txul_5011_defconfig
#}

#KERNEL_DEFCONFIG_karo-imx6ul-txul = "karo_imx6ul_txul_5011_defconfig"
#KBUILD_DEFCONFIG_karo-imx6ul-txul = "karo_imx6ul_txul_5011_defconfig"

# <-- custom defconfig hardcoded
# --> defconfig - seems to work better with config fragments than above

SRC_URI += "\
            file://defconfig \
           "
KERNEL_DEFCONFIG_karo-imx6ul-txul = ""
KBUILD_DEFCONFIG_karo-imx6ul-txul = ""
# <-- defconfig

# --> config fragments
FRAGMENTPATH="${THISDIR}/fragments"
FILESEXTRAPATHS_prepend := "${FRAGMENTPATH}:"

SRC_URI += "\
	    file://no-input-evbug.cfg \
            file://no-debug-pinctrl.cfg \
            file://no-debug-gpio.cfg \
           "
# <-- config fragments

# We have upstream device trees and
# I made a separate recipe for device trees see in recipes-bsp
