# Copyright (C) 2018 genius user <genius@ReliableEmbeddedSystems.com>
# Released under the MIT license (see COPYING.MIT for the terms)

SUMMARY = "karo specific device tree"
DESCRIPTION = "karo specific BSP device trees from within layer."
SECTION = "bsp"

# the device trees from within the layer are licensed as MIT, kernel includes are GPL
LICENSE = "MIT & GPLv2"
LIC_FILES_CHKSUM = " \
                file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302 \
                file://${COMMON_LICENSE_DIR}/GPL-2.0;md5=801f80980d171dd6425610833a22dbe6 \
                "

inherit devicetree

# device tree sources for the various machines
COMPATIBLE_MACHINE_karo-imx6ul-txul = ".*"

# --> karo 5010/5011 

50105011COMMONPATH="${THISDIR}/5010-5011-common"
FILESEXTRAPATHS_prepend := "${50105011COMMONPATH}:"

SRC_URI_append_karo-imx6ul-txul = " file://imx6ul-txul-mainboard.dtsi \
                                    file://karo-imx6ul-tx6ul.dtsi \
                                  "
# <-- karo 5010/5011

# --> karo 5011

5011PATH="${THISDIR}/5011"
FILESEXTRAPATHS_prepend := "${5011PATH}:"

# -->
# if you have it on a mainboard add this as well:
# SRC_URI_append_karo-imx6ul-txul = " file://imx6ul-txul-5011-mainboard.dts 
#                                   "
# <--

SRC_URI_append_karo-imx6ul-txul = " file://karo-imx6ul-tx6ul-0011.dts \
                                  "

# <-- karo 5011

# --> karo 5010

5010PATH="${THISDIR}/5010"
FILESEXTRAPATHS_prepend := "${5010PATH}:"

# -->
# if you have it on a mainboard add this as well:
SRC_URI_append_karo-imx6ul-txul = " file://imx6ul-txul-5010-mainboard.dts \
                                   "
# <--

SRC_URI_append_karo-imx6ul-txul = " file://karo-imx6ul-tx6ul-0010.dts \
                                  "
# <-- karo 5010

# --> bfe 0010

0010BFEPATH="${THISDIR}/0010-bfe"

FILESEXTRAPATHS_prepend := "${0010BFEPATH}:"

SRC_URI_append_karo-imx6ul-txul = " file://imx6ul-tx6ul-0010-bfe.dts \
                                    file://bfe-imx6ul-tx6ul-0010.dtsi \
                                    file://bfe-imx6ul-tx6ul.dtsi \
                                    file://bfe-imx6ul-txul-mainboard.dtsi \
				  "
# <-- bfe 0010                    
